import { createApp } from 'vue'
import App from '@/App.vue'
import router from '@/router/IndexRouter'
import store from '@/store/IndexStore'

import ElementPlus from 'element-plus'
import zhCn from 'element-plus/es/locale/lang/zh-cn'
import 'element-plus/dist/index.css'
// import 'element-plus/theme-chalk/display.css'
import "@/assets/css/tailwind.css"

/* Core CSS required for Ionic components to work properly */
import '@ionic/vue/css/core.css';
/* Basic CSS for apps built with Ionic */
// import '@ionic/vue/css/normalize.css';
// import '@ionic/vue/css/structure.css';
// import '@ionic/vue/css/typography.css';
/* Optional CSS utils that can be commented out */
// import '@ionic/vue/css/padding.css';
// import '@ionic/vue/css/float-elements.css';
// import '@ionic/vue/css/text-alignment.css';
// import '@ionic/vue/css/text-transformation.css';
// import '@ionic/vue/css/flex-utils.css';
// import '@ionic/vue/css/display.css';


const app = createApp(App)

import Vue3TouchEvents from "vue3-touch-events";
app.use(Vue3TouchEvents);

import { IonicVue } from '@ionic/vue';
app.use(IonicVue)

// import axios from './utils/http'
// import { AxiosInstance } from 'axios'
// declare module "@vue/runtime-core" {
//   interface ComponentCustomProperties {
//     axios: AxiosInstance;
//   }
// }
// app.config.globalProperties.axios = axios
// vm的直接属性  this.$option
// app.config.optionMergeStrategies.title = function (toVal, fromVal) {
//   console.log('merge title ',fromVal, toVal)
//   // => "goodbye!", undefined
//   // => "hello", "goodbye!"
//   return fromVal + '-' + toVal
// }
app.use(store)
app.use(router)
app.use(ElementPlus,{
  locale: zhCn
})

router.isReady().then(() => {
  app.mount('#app');
});
// app.mount('#app')