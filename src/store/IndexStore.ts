import { createStore } from 'vuex'
import UserStore from "./UserStore";
import router from "@/router/IndexRouter";
import { getLocal,setLocal as local,MUTION } from "@/utils/common";

export default createStore({
  state: {
    isLogin: getLocal('isLogin') || false,
    uinfo: getLocal('uinfo') || null,
    todes: [] as TODO[]
  },
  // 计算属性
  // getters:{
  // },
  mutations: {
     initTodo(state,payload:TODO[]){
       state.todes = payload
     },
     addTodo(state,payload:TODO){
        state.todes.push(payload)
     },
     [MUTION.LOGIN](state,payload) {
       console.log("store - "+ MUTION.LOGIN,state,payload);
       state.uinfo = payload
       local('uinfo',payload)
       state.isLogin = true
       local('isLogin',true)
      },
      [MUTION.LOGOUT] (state,payload) {
        console.log("store - "+ MUTION.LOGOUT,state,payload);
        state.uinfo = null
        local('uinfo',null)
        state.isLogin = false
        local('isLogin',false)
        router.push('/login')
     }
  },
  // 异步事件
  actions: {
     initTodo({ commit }){
        // settimeout
     },
     initTodo2(ctx){
      //  ctx.commit('')
        // settimeout
     },
     addTode({ commit , state},payload : string) {
        commit("addTodo",{
          id: 1
        })
     }
  },
  modules: {
    user: UserStore
  }
})

declare type TODO = {
  id:number
} | null

declare type UINFO = {
  id:number,
  name: string,
  pass: string,
  sid: string
} | null
