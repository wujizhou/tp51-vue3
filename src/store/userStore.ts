export default {
  namespaced: true,
  state: {
    userName: 'userTest',
  },
  mutations: {
    test(state, payload): void {
      console.log("user store - ", state, payload);
      state.userName = payload.name
    },
  },
  actions: {
  },
  modules: {
  }
}
