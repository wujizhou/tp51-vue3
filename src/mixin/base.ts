import router from '@/router/IndexRouter'
import store from '@/store/IndexStore'
import axios from '@/utils/http'
import MyUpdCol from '@/comps/MyUpdCol.vue'
import MyDelCol from '@/comps/MyDelCol.vue'
import MyElPager from '@/comps/MyElPager.vue'

declare type BASE_DATA = {
  // eslint-disable-next-line
  system: any, isLogin: boolean, uinfo: any, title: string
}
declare type BASE_DATA_TABLE = {
  // eslint-disable-next-line
  myTableData: any[], myPager: { page: 1, size: 10 }, myParams?: {}, myTotal?: 0,mySort: {prop:'id',order:'descending'}
}
declare type TABLE_PARAMS = {
  page: number,
  size: number,
  order: string,
  field: string,
  // eslint-disable-next-line
  [key:string]: any
}

const dtSexs = new Map<number,string>()
dtSexs.set(1, '男')
dtSexs.set(2, '女')
const dtRoles = new Map<number,string>()
dtRoles.set(1, '管理员')
dtRoles.set(2, '默认用户')
dtRoles.set(3, '商铺管理组')

const UNCHECK_APIS = ["/login"]
const baseTable = {
  data: function() : BASE_DATA_TABLE {
    return {
      myTableData: [],
      myParams: {},
      myTotal: 0,
      myPager: {
        page: 1,
        size: 10,
      },
      mySort: {
        prop: 'id',
        order: 'descending'
      },
    }
  },
  components: {
    MyDelCol,
    MyUpdCol,
    MyElPager
  },
  computed: {
    myHttpParams(): TABLE_PARAMS {
      return Object.assign({
        page: this.myPager.page,
        size: this.myPager.size,
        order: this.mySort.order.slice(0, -6),
        field: this.mySort.prop,
      }, this.myParams);
    },
  }
  // 不要用箭头函数
  ,watch: {
    myPager: {
      // eslint-disable-next-line
      handler(val, oldVal): void {
        // console.log('params.page', val, oldVal);
        this.getTableData();
      },
      deep: true,
      immediate: true,
    },
    params: {
      deep: true,
      // eslint-disable-next-line
      handler(val, oldVal) {
        // console.log('params.kword', val, oldVal);
        this.getTableData();
      },
    },
  },
  methods: {
    pageChange: function (page: number, size: number) : void {
      console.log('father page change', page, size, this.pager);
      this.myPager = { page, size }
    },
    sortChange({ prop, order }: { prop: string, order: string }): void {
      this.mySort = { order,prop }
      this.getTableData()
    },
    getTableData(): void {
      // console.log(this.params,this.urlencode(this.pararms,true));
      // console.log("getTableData", this.tableData, this.params);
      axios.post(this.api.LIST + this.urlEncode(this.myHttpParams)).then(rsp => {
        // axios.post(this.api,this.params).then(rsp => {
        this.myTableData = rsp.data
        // eslint-disable-next-line
        this.myTotal = (rsp as any).count
        console.log('rsp', rsp);
      })
    },
    // eslint-disable-next-line
    updOne(field: string, row): void {
      console.log('filed', field, row)
      // TODO ajax
      // axios.post(this.api.UPDONE + this.urlEncode(this.params)).then(rsp => {
      //   console.log('rsp', rsp);
      //   this.getTableData()
      // })
    },
    // eslint-disable-next-line
    delOne(field: string, row): void {
      console.log('filed', field, row)
      // TODO ajax
      // axios.post(this.api.DELS + this.urlEncode(this.params)).then(rsp => {
      //   console.log('rsp', rsp);
      //   this.getTableData()
      // })
    },
  }
}
const base = {
  data: function() :BASE_DATA {
    return {
      system: {
        author: 'rainbow<977746075@qq.com>',
        name: 'EBUY电商',
        dtRoles,
        dtSexs,
      },
      isLogin: store.state.isLogin || false,
      uinfo: store.state.uinfo || null,
      title: '全新启航,结伴远行?',
    }
  },
  created() : void  {
    if (!this.isLogin) {
      const path = router.currentRoute.value.fullPath;
      console.log("123",path);
      if (!UNCHECK_APIS.includes('/login')){
        console.log('need login : ',this.isLogin,path);
        router.push('/login')
      }
    }
  },
  methods: {
    // eslint-disable-next-line
    json(obj ) : string{
      return obj ? JSON.stringify(obj) : '';
    },
    time(num: number) : void {
      // eslint-disable-next-line
      if (!(Date.prototype as any).format) {
        // eslint-disable-next-line
        (Date.prototype as any).format = function (fmt: string) { //author: meizz
          //yyyy/MM/dd hh:mm:ss
          // eslint-disable-next-line
          const o: {[key:string]:any} = {
            "M+": this.getMonth() + 1, //月份
            "d+": this.getDate(), //日
            "h+": this.getHours(), //小时
            "m+": this.getMinutes(), //分
            "s+": this.getSeconds(), //秒
            "q+": Math.floor((this.getMonth() + 3) / 3), //季度
            "S": this.getMilliseconds() //毫秒
          };
          if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
          for (const k in o)
            if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
          return fmt;
        };
      }
      // eslint-disable-next-line
      return (new Date(num * 1000) as any).format('yyyy/MM/dd hh:mm');
    },
    // eslint-disable-next-line
    urlEncode(obj) : string {
      const params = Object.keys(obj).map(key => {
        return encodeURIComponent(key) + '=' + encodeURIComponent(obj[key])
      }).join('&')
      return params ? '?' + params : ''
    },
    // eslint-disable-next-line
    dtRoles(rid: number) : String{
      switch (rid) {
        case 1:
          return '管理员';
        case 2:
          return '默认用户';
        case 3:
          return '商铺管理组';
        default:
          return '-';
      }
    },
    // eslint-disable-next-line
    dtClass(prefix,id) : string {
      return `dt-${prefix}-${id}`
    },
    // eslint-disable-next-line
    dtSexs(rid: number): String{
      switch (rid) {
        case 1:
          return '男';
        case 2:
          return '女';
        default:
          return '-';
      }
    },
  }
}

export default base
export {
  base,
  baseTable
}