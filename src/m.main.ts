import { createApp } from 'vue'
import App from '@/m.vue'
// import router from '@/router/IndexRouter'
// import store from '@/store/IndexStore'

// import ElementPlus from 'element-plus'
// import zhCn from 'element-plus/es/locale/lang/zh-cn'
import Vue3TouchEvents from "vue3-touch-events";

import 'element-plus/dist/index.css'
import "@/assets/css/tailwind.css"
const app = createApp(App)
app.use(Vue3TouchEvents);

// import axios from './utils/http'
// import { AxiosInstance } from 'axios'
// declare module "@vue/runtime-core" {
//   interface ComponentCustomProperties {
//     axios: AxiosInstance;
//   }
// }
// app.config.globalProperties.axios = axios
// vm的直接属性  this.$option
// app.config.optionMergeStrategies.title = function (toVal, fromVal) {
//   console.log('merge title ',fromVal, toVal)
//   // => "goodbye!", undefined
//   // => "hello", "goodbye!"
//   return fromVal + '-' + toVal
// }
// app.use(store)
// app.use(router)
// app.use(ElementPlus,{
//   locale: zhCn
// })
app.mount('#app')