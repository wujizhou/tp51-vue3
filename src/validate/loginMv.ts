const data = {
  loginForm: {
    uname: 'rainbow',
    upass: '2613747851',
    remember: false,
  },
}

const rules = {
  uname: [{
    required: true,
    message: '需要用户名',
    trigger: 'blur'
  }, {
    min: 4,
    max: 10,
    message: 'need 4-12 charaters',
  }
  ],
  upass: [{
    required: true,
    message: '需要密码',
    trigger: 'blur'
  }],
}
export default {}
export { data,rules }