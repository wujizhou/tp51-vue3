import { ElMessage } from 'element-plus'

export const info = (msg : string) : void => {
    ElMessage(msg)
}
export const err = (msg : string) : void => {
  ElMessage.error(msg)
}
export const suc = (msg : string) : void => {
  ElMessage.success({
    message: msg,
    type: 'success',
  })
}

export default info