import { API,MUTION } from '@/utils/cons'
// 获取vue的 env
const getEnv = (name : string) : string => {
  return process.env['VUE_APP_' + name];
}
// eslint-disable-next-line
const getLocal = (key: string): any => { // localStorage存/取
  if(!key) return '';
  // app_id 不得有数字字母外
  // key = layui.cache.rainbow.app_id + '|'+ layui.cache.rainbow.rev_ver + '|' + key;
  return JSON.parse(localStorage.getItem(key) || 'null');
}

// eslint-disable-next-line
const query = (s: string):any => {
    return document.querySelector(s)
}
// eslint-disable-next-line
const queryAll = (s: string): any => {
    return document.querySelectorAll(s)
}
// eslint-disable-next-line
const setLocal = (key: string, val:any): void => { // localStorage存/取
  if(!key) return ;
  // app_id 不得有数字字母外
  // key = layui.cache.rainbow.app_id + '|'+ layui.cache.rainbow.rev_ver + '|' + key;
  localStorage.setItem(key, JSON.stringify(val));
}

export { API, MUTION,getEnv,getLocal,setLocal,query,queryAll }
export default getEnv