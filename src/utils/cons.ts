// APIS
const PREFIX = '/api/'
const API = {
  LOGIN : PREFIX + 'login/ajax',
  USER : {
    MENU: PREFIX + 'api_menu/ajax',  // 用户菜单
    LIST   : PREFIX + 'api_user/index',
    DEL    : PREFIX + 'api_user/dels',
    UPDONE : PREFIX + 'api_user/upd',
  },
  MENU : {
    LIST: PREFIX + 'api_menu/index', // 菜单列表
    DEL: PREFIX + 'api_menu/dels',
    UPDONE: PREFIX + 'api_menu/upd',
  }
}

// vuex Mutations
const MUTION = {
  LOGIN : 'login',
  LOGOUT : 'logout'
}
export { API,MUTION }
