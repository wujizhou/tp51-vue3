// import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import { createRouter, createWebHistory } from '@ionic/vue-router'
// import Index from '../views/Index.vue'
import Home from '../views/Home.vue'
import Err from '../views/Err.vue'
import Login from '../views/Login.vue'
import User from '../views/User.vue'
import Menu from '../views/Menu.vue'

// import 只能静态导入
// const VIEWS = '../views/'
// const view = function(name:string): string {
//   return VIEWS + name + '.vue';
// }
// const COMPS = '../components/'
// const comp = function (name: string): string {
//   return COMPS + name + '.vue';
// }
// const routes: Array<RouteRecordRaw> = [
// eslint-disable-next-line
const routes: Array<any> = [
  {
    path: '/',
    redirect: '/home',
  },
  {
    path: '/login',
    component: () => Login
  },
  {
    path: '/home',
    component: Home,
    children: [
      {
        path: 'user',
        name: 'user',
        component: User,
      },
      {
        path: 'menu',
        name: 'menus',
        component: Menu,
      }
    ]
  },
  {
    path: '/err', component: Err
  },
  {
    path: '/:catchAll(.*)', redirect: '/err'
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
