module.exports = {
  pages: {//配置多页面入口
    m: {
      entry: 'src/m.main.ts',
      template: 'public/m.html',
    },
    index: {
      entry: 'src/main.ts',
      template: 'public/index.html',
    },
  },
  devServer: {
    // open: true, //自动打开浏览器
    // open: process.env == 'develop',
    // host: 'localhost',
    // port: '8080',
    // https: false,
    // hotonly: false,
    proxy: {
      '/api': {
        target: 'http://ebuy.my/admin.php',
        ws:true,
        secure: false,
        changeOrigin: true, //是否跨域
        pathRewrite: {
           '^/api': ''
        }
      }
    }
  }
}